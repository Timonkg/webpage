let leagues;
let countries = [];

function showForm() {
    document.getElementById("popup").style.display = "block";
}

function closeForm() {
    document.getElementById("popup").style.display = "none";
}

function postLeague() {
    let name = document.getElementById("name").value;
    let country = document.getElementById("country").value;
    let imageURL = document.getElementById("imageurl").value;

    const league = {
        name: name,
        country: country,
        imageURL: imageURL
    }


    console.log(JSON.stringify(league))

    fetch('http://localhost:8080/leagues', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(league),
    })
        .then(response => response.json())
        .then(data => {
            refreshLeagues()
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

async function getLeagues(country) {
    if (country === undefined){
        const response = await fetch('http://localhost:8080/leagues')
        const data = await response.json();
        return data;
    }else {
        const response = await fetch('http://localhost:8080/leagues?country='+country)
        const data = await response.json();
        return data;
    }

}

async function onInit() {
    console.log('test')
    leagues = await getLeagues()
    getCountries()
    printLeagues()
    printCountries()
}

function printLeagues() {
    removeElementsByClass("card")
    if (leagues.length > 0) {
        leagues.forEach(league => {
            let card = document.createElement("div")
            card.classList.add("card")

            let cardContent = document.createElement("div")
            cardContent.classList.add("card__content")
            cardContent.setAttribute("id", league.id)
            cardContent.addEventListener('click', function () {
                changeWindow(cardContent.getAttribute("id"))
            })

            let image = document.createElement("img")
            image.classList.add("image")
            image.src = league.imageURL

            let h3 = document.createElement('H3')
            let h3text = document.createTextNode(league.name)
            h3.append(h3text);

            let p = document.createElement("P")
            let ptext = document.createTextNode(league.country)
            p.append(ptext)

            cardContent.append(image, h3, p)
            card.appendChild(cardContent)

            let cardContainer = document.getElementById("card__container")
            console.log(cardContainer)
            cardContainer.appendChild(card)
        })
    } else console.log('No leagues')
}

function printCountries(){
    countries.forEach(country =>{
        let li = document.createElement('li')
        li.setAttribute('id', country)
        let liText = document.createTextNode(country)
        li.append(liText)

        li.addEventListener('click',async function () {
            leagues = await getLeagues(country)
            printLeagues()
        })

        document.getElementById('dropdown').append(li)
    })

}

function removeElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}

function changeWindow(leagueId) {
    localStorage.setItem("leagueid", leagueId)
    window.location.href = "html/leagueDetailPage.html"
}

function dropDown(){
    let list = document.getElementById('dropdown')
    if(list.style.display === 'none'){
        list.style.display = 'block'
    }else list.style.display = 'none'
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function getCountries(){
    leagues.forEach(league =>{
        countries.push(league.country)
    })
    countries = countries.filter(onlyUnique)
}

async function refreshLeagues(){
    leagues = await leagues
    printLeagues()
}

async function getLeaguesByName(name){
    const response = await fetch('http://localhost:8080/leagues?name='+name)
    const data = await response.json();
    return data;
}

async function printSearchedLeague(){
    let name = document.getElementById('search').value
    document.getElementById('search').value = ''
    leagues = await getLeaguesByName(name)
    if (leagues.length > 0){
        printLeagues()
    }else alert('League with name: ' + name + ', not found')
}

