let teams
let team = {}
team.players = []

function showPostForm() {
    document.getElementById('popup').style.display = 'block';

    let popupTitle = document.getElementById('popup-title')
    popupTitle.innerHTML = ''
    let popupTitleText = document.createTextNode('Nieuw team toevoegen')
    popupTitle.append(popupTitleText)

    document.getElementById('name').value = ''
    document.getElementById('points').value = ''
    document.getElementById('imageurl').value = ''

    let button = document.getElementById('submit')
    button.addEventListener('click', postTeam)
}

function showPutForm(teamId) {
    let team = getTeamById(teamId)

    document.getElementById('popup').style.display = 'block';

    let popupTitle = document.getElementById('popup-title')
    popupTitle.innerHTML = ''
    let popupTitleText = document.createTextNode('Team veranderen')
    popupTitle.append(popupTitleText)

    document.getElementById('name').value = team.name
    document.getElementById('points').value = team.points
    document.getElementById('imageurl').value = team.imageURL

    let button = document.getElementById('submit')
    button.value = 'Veranderen'
    button.addEventListener('click', function () {
        putTeam(teamId)
    })
}

function closeForm() {
    document.getElementById('popup').style.display = 'none';
}


function postTeam() {

    let name = document.getElementById('name').value;
    let points = document.getElementById('points').value;
    let imageURL = document.getElementById('imageurl').value;

    const team = {
        name: name,
        points: points,
        imageURL: imageURL
    }
    fetch('http://localhost:8080/leagues/' + localStorage.getItem('leagueid') + '/teams', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(team),
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            refreshTeams();
            closeForm()
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

async function putTeam(teamId) {
    let name = document.getElementById('name').value;
    let points = document.getElementById('points').value;
    let imageURL = document.getElementById('imageurl').value;

    team = {
        name: name,
        points: points,
        imageURL: imageURL
    }
    const response = await fetch('http://localhost:8080/teams/' + teamId, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(team)
    })
    const data = await response.json()
    await refreshTeams();
    closeForm()
    return data;
}

async function getLeague() {
    const response = await fetch('http://localhost:8080/leagues/' + localStorage.getItem('leagueid'));
    const data = await response.json();
    return data;
}

async function getTeams() {
    const response = await fetch('http://localhost:8080/teams');
    const data = await response.json();
    return data;
}

function getTeamById(teamId) {
    return teams.find(team => team.id == teamId)
}

async function onInit() {
    league = await getLeague()
    teams = league.teams
    sortTeams(teams);
    printTeams(teams, league)
    printPage(league)
}

async function onInitAllTeams() {
    teams = await getTeams()
    printTeams(teams)
}

async function refreshTeams() {
    let league = await getLeague();
    teams = league.teams
    sortTeams(teams);
    printTeams(teams)
}

function printTeams(teams) {
    removeElementsByClass('team_row', 'title', 'subtitle', 'img')
    let counter = 1;

    teams.forEach(team => {
        let th = document.createElement('th');
        th.classList.add('click_show_team')
        let thText = document.createTextNode('' + counter);
        th.append(thText)

        let imageTd = document.createElement('td')
        let image = document.createElement('img')
        image.src = team.imageURL
        image.classList.add('row_image', 'click_show_team')
        imageTd.append(image)

        let teamNameTd = document.createElement('td')
        teamNameTd.classList.add('click_show_team')
        let teamNameTdText = document.createTextNode(team.name)
        teamNameTd.append(teamNameTdText)

        let teamPointsTd = document.createElement('td')
        teamPointsTd.classList.add('click_show_team')
        let teamPointsTdText = document.createTextNode(team.points)
        teamPointsTd.append(teamPointsTdText);

        let editTd = document.createElement('td')
        let editTdIcon = document.createElement('i')
        editTdIcon.classList.add('fa-solid', 'fa-pen-to-square', 'fa-2xl')
        editTd.append(editTdIcon)
        editTd.addEventListener('click', function () {
            showPutForm(tr.getAttribute('id'))
        })

        let deleteTd = document.createElement('td')
        let deleteTdIcon = document.createElement('i')
        deleteTdIcon.classList.add('fa-solid', 'fa-trash-can', 'fa-2xl')
        deleteTd.addEventListener('click', function () {
            deleteTeam(tr.getAttribute('id'))
        })
        deleteTd.append(deleteTdIcon)

        let tr = document.createElement('tr')
        tr.id = team.id
        tr.classList.add('team_row')
        tr.append(th, imageTd, teamNameTd, teamPointsTd, editTd, deleteTd)
        document.getElementById('tbody_teams').append(tr)

        counter++;
    })

    document.querySelectorAll('.click_show_team').forEach(item => {
        item.addEventListener('click', function () {
            let tr = item.parentElement
            localStorage.setItem('teamid', tr.getAttribute('id'))
            showTeam(tr.getAttribute('id'))
        })
    })

}

function printPage(league) {
    let img = document.createElement('img')
    img.classList.add('img')
    img.src = league.imageURL


    let h1 = document.createElement('h1')
    h1.classList.add('title')
    let h1text = document.createTextNode(league.name)
    h1.append(h1text)

    let h3 = document.createElement('h3')
    h3.classList.add('subtitle')
    let h3text = document.createTextNode(league.country)
    h3.append(h3text)

    let content_header_body = document.getElementById('content_header_body')
    content_header_body.append(h1, h3)

    document.getElementById('content_header').insertBefore(img, content_header_body)
}

function removeElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}

async function deleteTeam(id) {
    fetch('http://localhost:8080/teams/' + id, {
        method: 'DELETE',
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            refreshTeams();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function sortTeams(teams) {
    teams.sort(function (a, b) {
        if (a.points < b.points) {
            return 1;
        }
        if (a.points > b.points) {
            return -1;
        } else return 0;
    })
    return teams;
}

function deleteLeague() {
    fetch('http://localhost:8080/leagues/' + localStorage.getItem('leagueid'), {
        method: 'DELETE',
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            window.location.replace('index.html')
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function showTeam(teamId) {
    document.getElementById('team_popup').style.display = 'block';

    removeElementsByClass('player_row')

    teams.forEach(t => {
        if (t.id == teamId) {
            team = t;
        }
    })
    if (team.players.length > 0) {
        team.players.forEach(player => {
            let lastName = document.createElement('th');
            let lastNameText = document.createTextNode(player.lastName);
            lastName.append(lastNameText)

            let firstName = document.createElement('td');
            let firstNameText = document.createTextNode(player.firstName);
            firstName.append(firstNameText)

            let position = document.createElement('td')
            switch (player.position){
                case 'Striker': position.append(document.createTextNode('Aanvaller')); break
                case 'Midfielder': position.append(document.createTextNode('Middenvelder')); break
                case 'Defender': position.append(document.createTextNode('Verdediger'));break
            }


            let age = document.createElement('td')
            let ageText = document.createTextNode('11')
            age.append(ageText)

            let tr = document.createElement('tr')
            tr.classList.add('player_row')
            tr.append(lastName,firstName,position,age)

            let tbody = document.getElementById('team_tbody')

            let last_row = document.getElementById('last_row')
            tbody.insertBefore(tr,last_row)
        })


    } else console.log('Team does not have players')
}

function closeTeam() {
    document.getElementById('team_popup').style.display = 'none';
}

function addPlayer() {
    let lastName = document.getElementById('lastName').value;
    let firstName = document.getElementById('firstName').value;
    let position = document.getElementById('position').value;
    let birthDate = document.getElementById('birthDate').value;

    let player = {
        firstName: firstName,
        lastName: lastName,
        position: position,
        birthDate: birthDate
    }

    fetch('http://localhost:8080/teams/' + localStorage.getItem('teamid') + '/players' , {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(player),
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            team.players.push(player)
            showTeam(localStorage.getItem('teamid'))
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}
