let players;

async function getAllPlayers() {
    const resp = await fetch('http://localhost:8080/players')
    const data = await resp.json()
    players = await data
}

async function onInit() {
    await getAllPlayers()
    await showPlayers()
}

function showPlayers() {

    removeElementsByClass('player_row')

    if (players.length > 0) {
        players.forEach(player => {
            let tr = document.createElement('tr')
            tr.classList.add('player_row')
            tr.setAttribute('id', player.id)

            let lastName = document.createElement('th');
            let lastNameText = document.createTextNode(player.lastName);
            lastName.append(lastNameText)

            let firstName = document.createElement('td');
            let firstNameText = document.createTextNode(player.firstName);
            firstName.append(firstNameText)

            let position = document.createElement('td')
            switch (player.position) {
                case 'Striker':
                    position.append(document.createTextNode('Aanvaller'));
                    break
                case 'Midfielder':
                    position.append(document.createTextNode('Middenvelder'));
                    break
                case 'Defender':
                    position.append(document.createTextNode('Verdediger'));
                    break
            }

            let age = document.createElement('td')
            let ageText = document.createTextNode('11')
            age.append(ageText)

            let editTd = document.createElement('td')
            let editTdIcon = document.createElement('i')
            editTdIcon.classList.add('fa-solid', 'fa-pen-to-square', 'fa-lg')
            editTd.append(editTdIcon)
            editTd.addEventListener('click', function () {
                changeRowToEdit(tr.getAttribute('id'))
            })

            let deleteTd = document.createElement('td')
            let deleteTdIcon = document.createElement('i')
            deleteTdIcon.classList.add('fa-solid', 'fa-trash-can', 'fa-lg')
            deleteTd.append(deleteTdIcon)
            deleteTd.addEventListener('click', function () {
                deletePlayer(tr.getAttribute('id'))
            })


            tr.append(lastName, firstName, position, age, editTd, deleteTd)

            let tbody = document.getElementById('team_tbody')

            let last_row = document.getElementById('last_row')
            tbody.insertBefore(tr, last_row)
        })


    } else console.log('No players')
}

function removeElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}

async function deletePlayer(id) {
    const response = await fetch('http://localhost:8080/players/' + id, {
        method: 'DELETE',
    })
    const data = await response.json();
    await onInit()
    return data
}

function changeRowToEdit(id) {
    showPlayers()
    let tr = document.getElementById(id)
    let player = getPlayerById(id)
    while (tr.firstChild) {
        tr.removeChild(tr.firstChild)
    }
    console.log(player)

    let lastName = document.createElement("th")
    let lastNameInput = document.createElement('input')
    lastNameInput.classList.add('new_player_input')
    lastNameInput.value = player.lastName
    lastNameInput.id = 'lastName'
    lastName.append(lastNameInput)


    let firstName = document.createElement("td")
    let firstNameInput = document.createElement('input')
    firstNameInput.classList.add('new_player_input')
    firstNameInput.value = player.firstName
    firstNameInput.id = 'firstName'
    firstName.append(firstNameInput)

    let position = document.createElement('td')
    let positionInput = document.createElement('input')
    positionInput.classList.add('new_player_input')
    positionInput.value = player.position
    positionInput.id = 'position'
    position.append(positionInput)

    let birthDate = document.createElement('td')
    let birthDateInput = document.createElement('input')
    birthDateInput.type = 'date'
    birthDateInput.classList.add('new_player_input')
    birthDateInput.id = 'birthDate'
    birthDate.append(birthDateInput)

    let saveButton = document.createElement('td')
    let saveButtonIcon = document.createElement('i')
    saveButtonIcon.classList.add('fa-solid', 'fa-circle-plus', 'fa-xl', 'add_player_button');
    saveButton.append(saveButtonIcon)
    saveButton.addEventListener('click', function () {
        putPlayer(id)
        showPlayers()
    })

    let cancelButton = document.createElement('td')
    let cancelButtonIcon = document.createElement('i')
    cancelButtonIcon.classList.add('fa-solid', 'fa-x', 'fa-xl', 'cancel_button')
    cancelButton.append(cancelButtonIcon)
    cancelButton.addEventListener('click', onInit)

    tr.append(lastName,firstName,position,birthDate,saveButton, cancelButton)
}

function getPlayerById(id) {
    let returnPlayer
    players.forEach(player => {
        if (player.id == id) returnPlayer = player;
    })
    return returnPlayer;
}

async function putPlayer(id) {
    let pIndex = players.findIndex((obj => obj.id == id));

    const player = {
        lastName: document.getElementById('lastName').value,
        firstName: document.getElementById('firstName').value,
        position: document.getElementById('position').value,
        birthDate: document.getElementById('birthDate').value,
        id: id
    }
    console.log(players[pIndex])
    players[pIndex] = player

    console.log(players)

    const response = await fetch('http://localhost:8080/players/' + id, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(player)
    })
    const data = response.json()
    return data;
}